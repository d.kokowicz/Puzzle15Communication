//
//  GameVCPresenter.swift
//  Puzzle15Communication
//
//  Created by Dominika Kokowicz on 01/06/2021.
//

import UIKit

protocol GameVCPresenterDelegate: AnyObject {
  func modelDidCreate(_ model: BoardModel)
  func moveTiles(id: Int, movesCount: Int)
  func presentWinAlert()
  func movesCountDidReset(_ movesCount: Int)
}

class GameVCPresenter {
  weak var delegate: GameVCPresenterDelegate?
  private var movesCount = 0
  private let basePositions = Array(1...Constant.boardSize * Constant.boardSize)
  private var currentPositions = Array(1...Constant.boardSize * Constant.boardSize)
  private var valuesSurroundingIdTap = [Int]()
  
  private func getIndexOfZero() -> Int? {
    if let indexfOfZero = currentPositions.firstIndex(of: Constant.valueOfBlank) {
      return indexfOfZero
    } else {
      return nil
    }
  }
  
  private func getLeftRange(for number: Int) -> Int {
    return number * Constant.boardSize + 1
  }
  
  private func getRightRange(for number: Int) -> Int {
    return getLeftRange(for: number) + Constant.boardSize - 1
  }
  
  private func getCurrentLeftValue(for id: Int) -> Int? {
    guard let indexOfId = currentPositions.firstIndex(of: id) else { return nil }
    let leftIndex = indexOfId - 1
    for number in 0..<Constant.boardSize {
      if indexOfId == (getLeftRange(for: number) - 1) {
        return nil
      }
    }
    if currentPositions.indices.contains(leftIndex) {
      return currentPositions[leftIndex]
    }
    return nil
  }
  
  private func getCurrentRightValue(for id: Int) -> Int? {
    guard let indexOfId = currentPositions.firstIndex(of: id) else { return nil }
    let rightIndex = indexOfId + 1
    for number in 0..<Constant.boardSize {
      if indexOfId == (getRightRange(for: number) - 1) {
        return nil
      }
    }
    if currentPositions.indices.contains(rightIndex) {
      return currentPositions[rightIndex]
    }
    return nil
  }
  
  private func getCurrentBottomValue(for id: Int) -> Int? {
    guard let indexOfId = currentPositions.firstIndex(of: id) else { return nil }
    let bottomIndex = indexOfId + Constant.boardSize
    if currentPositions.indices.contains(bottomIndex) {
      return currentPositions[bottomIndex]
    }
    return nil
  }
  
  private func getCurrentTopValue(for id: Int) -> Int? {
    guard let indexOfId = currentPositions.firstIndex(of: id) else { return nil }
    let topIndex = indexOfId - Constant.boardSize
    if currentPositions.indices.contains(topIndex) {
      return currentPositions[topIndex]
    }
    return nil
  }
  
  private func getValuesSurroundingTap(for id: Int) {
    valuesSurroundingIdTap = [getCurrentLeftValue(for: id),getCurrentRightValue(for: id), getCurrentTopValue(for: id), getCurrentBottomValue(for: id) ].compactMap({$0})
  }
  
  private var isGameWon: Bool {
    return basePositions == currentPositions
  }
  
  private func findXCenter(id: Int, tileWidth: CGFloat) -> CGFloat {
    return CGFloat(((id - 1) % Constant.boardSize)) * (tileWidth)
  }
  
  private func findYCenter(id: Int, tileWidth: CGFloat) -> CGFloat {
    let row = CGFloat(((id - 1) / Constant.boardSize)).rounded(toPlaces: 0)
    return row * tileWidth
  }
  
  private func movesCountReset() {
    movesCount = 0
    delegate?.movesCountDidReset(movesCount)
  }
  
  func createModels(boardWidth: CGFloat) {
    shuffle()
    let tileWidth = boardWidth / CGFloat(Constant.boardSize)
    
    let tiles = basePositions.compactMap({ TileModel(id: currentPositions[$0-1], imageTile: UIImage(named: "\(currentPositions[$0-1])"), width: tileWidth, xCenter: findXCenter(id: $0, tileWidth: tileWidth), yCenter: findYCenter(id: $0, tileWidth: tileWidth))})
    
    let boardModel = BoardModel(tiles: tiles)
    
    delegate?.modelDidCreate(boardModel)
  }
  
  func shuffle() {
    movesCountReset()
    currentPositions = currentPositions.shuffled()
    
    if !isGameSolvable() {
      if getIndexOfZero() != 0 && getIndexOfZero() != 1 {
        let toSwap = currentPositions[0]
        currentPositions[0] = currentPositions[1]
        currentPositions[1] = toSwap
      } else {
        guard let lastIndex = currentPositions.indices.last else { return }
        let toSwap = currentPositions[lastIndex - 1]
        currentPositions[lastIndex - 1] = currentPositions[lastIndex]
        currentPositions[lastIndex] = toSwap
      }
    }
  }
  
  private func isGameSolvable() -> Bool {
    if Constant.boardSize % 2 == 0 {
      let n = countInversion() + blankRowNumber()
      return n % 2 == 0 ? false : true
    } else {
      return countInversion() % 2 == 0
    }
  }
  
  private func blankRowNumber() -> Int {
    for number in 0..<Constant.boardSize {
      guard let zeroIndex = getIndexOfZero() else { return 0 }
      let leftRange = getLeftRange(for: number)
      let rightRange = getRightRange(for: number)
      let range = (leftRange-1)...(rightRange-1)
      
      if range.contains(zeroIndex) {
        return number
      }
    }
    return 0
  }
  
  private func countInversion() -> Int {
    var inversionCount = 0
    
    for i in 0..<(Constant.boardSize * Constant.boardSize - 1) {
      for j in (i + 1)..<(Constant.boardSize * Constant.boardSize) {
        if currentPositions[j] != 0 &&
            currentPositions[i] != 0 &&
            currentPositions[i] > currentPositions[j] {
          inversionCount += 1
        }
      }
    }
    return inversionCount
  }
  
  private func increaseMovesCount() {
    movesCount += 1
  }
  
  private func tileCanBeMoved() -> Bool {
    return valuesSurroundingIdTap.contains(Constant.valueOfBlank)
  }
  
  private func ifWonPresentAlert() {
    if isGameWon {
      delegate?.presentWinAlert()
    }
    print(currentPositions)
  }
  
  private func updateMoveTilesInUI(for id: Int) {
    delegate?.moveTiles(id: id, movesCount: movesCount)
  }
  
  private func swapTiles(for id: Int) {
    guard let indexOfId = currentPositions.firstIndex(of: id), let zero = getIndexOfZero() else { return }
    
    currentPositions[indexOfId] = Constant.valueOfBlank
    currentPositions[zero] = id
  }
}

extension GameVCPresenter: BoardViewDelegate {
  func tileDidTap(id: Int) {
    valuesSurroundingIdTap = []
    getValuesSurroundingTap(for: id)
    
    if tileCanBeMoved() {
      swapTiles(for: id)
      increaseMovesCount()
      updateMoveTilesInUI(for: id)
      ifWonPresentAlert()
    }
  }
}

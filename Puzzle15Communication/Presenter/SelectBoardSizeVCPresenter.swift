//
//  SelectBoardSizeVCPresenter.swift
//  Puzzle15Communication
//
//  Created by Dominika Kokowicz on 18/06/2021.
//

import Foundation

protocol SelectBoardSizeVCPresenterDelegate: AnyObject {
}

class SelectBoardSizeVCPresenter {
  weak var delegate: SelectBoardSizeVCPresenterDelegate?

  func setBoardSize(for boardSize: Int) {
    Constant.boardSize = boardSize
  }
}

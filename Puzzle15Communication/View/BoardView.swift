//
//  BoardView.swift
//  Puzzle15Communication
//
//  Created by Dominika Kokowicz on 01/06/2021.
//

import UIKit

protocol BoardViewDelegate: AnyObject {
  func tileDidTap(id: Int)
}

final class BoardView: UIView {
  var size: Int?
  var width: CGFloat?
  weak var delegate: BoardViewDelegate?
  var tiles = [TileView]()
  
  func setup(model: BoardModel, delegate: BoardViewDelegate?, width: CGFloat?) {
    tiles.removeAll()
    self.subviews.forEach({ $0.removeFromSuperview() })
    self.size = model.size
    self.delegate = delegate
    self.layer.masksToBounds = true
    self.layer.borderWidth = 3
    self.layer.borderColor = UIColor.lightGray.cgColor
    UIView.animate(withDuration: 0.18) {
    for tileModel in model.tiles {
      let tileView = TileView(model: tileModel, delegate: self)
      self.tiles.append(tileView)
      self.addSubview(tileView)
    }
    }
  }
  
  func moveTile(id: Int) {
    let tile = tiles.first { $0.id == id }
    let zeroTile = tiles.first { $0.id == Constant.valueOfBlank }
    let tileCenter = tile?.center
    UIView.animate(withDuration: 0.3) {
      self.backgroundColor = .random
    tile?.center = zeroTile!.center
    zeroTile?.center = tileCenter!
    }
  }
}

extension BoardView: TileViewDelegate {
  func tileDidTap(id: Int) {
    delegate?.tileDidTap(id: id)
  }
}

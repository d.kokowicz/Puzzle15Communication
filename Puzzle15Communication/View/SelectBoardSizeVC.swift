//
//  SelectBoardSizeVC.swift
//  Puzzle15Communication
//
//  Created by Dominika Kokowicz on 17/06/2021.
//

import UIKit

class SelectBoardSizeVC: UIViewController {
  
  private let presenter = SelectBoardSizeVCPresenter()
  
  @IBAction func bs3ButtonTapped(_ sender: UIButton) {
    presenter.setBoardSize(for: 3)
  }
  
  @IBAction func bs4ButtonTapped(_ sender: UIButton) {
    presenter.setBoardSize(for: 4)
  }
  
  @IBAction func bs5ButtonTapped(_ sender: UIButton) {
    presenter.setBoardSize(for: 5)
  }
  
  @IBAction func bs6ButtonTapped(_ sender: UIButton) {
    presenter.setBoardSize(for: 6)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    navigationController?.isNavigationBarHidden = true
  }
}

extension SelectBoardSizeVC: SelectBoardSizeVCPresenterDelegate {
}


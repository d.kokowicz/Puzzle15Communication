//
//  TileView.swift
//  Puzzle15Communication
//
//  Created by Dominika Kokowicz on 01/06/2021.
//

import UIKit

protocol TileViewDelegate: AnyObject {
  func tileDidTap(id: Int)
}

final class TileView: UIView {
  weak var delegate: TileViewDelegate?
  var id: Int = 0
  var width: CGFloat?
  var xCenter: CGFloat?
  var yCenter: CGFloat?
  
  init(model: TileModel, delegate: TileViewDelegate?) {
    super.init(frame: CGRect(x: model.xCenter ?? 0, y: model.yCenter ?? 0, width: model.width ?? 0, height: model.width ?? 0))
    let imageView = UIImageView(frame: frame)
    imageView.image = model.imageTile
    self.tag = model.id
    imageView.isUserInteractionEnabled = true
    self.isUserInteractionEnabled = true
    imageView.center = CGPoint(x: self.frame.width / 2, y: self.frame.width / 2)
    imageView.layer.masksToBounds = true
    imageView.layer.borderWidth = 1
    imageView.layer.borderColor = UIColor.lightGray.cgColor
    self.delegate = delegate
    self.id = model.id
    self.width = model.width
    self.xCenter = model.xCenter
    self.yCenter = model.yCenter
    let tap = UITapGestureRecognizer(target: self, action: #selector(tileDidTap))
    self.addGestureRecognizer(tap)
    if self.id == Constant.valueOfBlank {
      self.alpha = 0.1
      imageView.image = UIImage(named: "blank")
    }
    
    addSubview(imageView)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  @objc func tileDidTap() {
    delegate?.tileDidTap(id: id)
  }
}

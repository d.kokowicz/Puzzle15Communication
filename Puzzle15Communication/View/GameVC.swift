//
//  ViewController.swift
//  Puzzle15Communication
//
//  Created by Dominika Kokowicz on 01/06/2021.
//

import UIKit

class GameVC: UIViewController {
  
  var seconds = 0
  var timer = Timer()

  private let presenter = GameVCPresenter() // 1. Create an instance of Presenter

  @IBOutlet weak var timerLabel: UILabel!
  @IBOutlet weak var movesLabel: UILabel!
  @IBOutlet weak var boardView: BoardView!
  
  @IBAction func shuffleButtonTapped(_ sender: UIBarButtonItem) {
    resetTimer()
    presenter.shuffle()
    presenter.createModels(boardWidth: boardView.frame.width)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    presenter.delegate = self // 2. Set a delegate
    self.navigationController?.isNavigationBarHidden = false
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    createTimer()
    startTimer()
    presenter.createModels(boardWidth: boardView.frame.width)
    // 3. Ask presenter to do its work on behalf of VC
  }
}

extension GameVC: GameVCPresenterDelegate {
  func movesCountDidReset(_ movesCount: Int) {
    movesLabel.text = String("Moves: \(movesCount)")
  }
  
  func presentWinAlert() {
    guard let movesText = movesLabel.text, let timeText = timerLabel.text else { return }
    let ac = UIAlertController(title: "Congratulations! You won!",
                               message: "\(movesText). Time: \(timeText)",
                               preferredStyle: .alert)
    ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
    self.present(ac, animated: true)
  }
  
  func modelDidCreate(_ model: BoardModel) {
    boardView.setup(model: model, delegate: self, width: boardView.frame.size.width)
  }
    //5. I got a model from presenter through protocol and can do view things I need with it
    //I can give boardView model attributes here
  func moveTiles(id: Int, movesCount: Int) {
    boardView.moveTile(id: id)
    movesLabel.text = String("Moves: \(movesCount)")
  }
}

extension GameVC: BoardViewDelegate {
  func tileDidTap(id: Int) {
    presenter.tileDidTap(id: id)
  }
}

//MARK: Timer
extension GameVC {
  func createTimer() {
    timer = Timer.scheduledTimer(
      timeInterval: 1,
      target: self,
      selector: #selector(startTimer),
      userInfo: nil,
      repeats: true)
    }
  
  @objc func startTimer() {
    seconds += 1
    let min = seconds / 60
    let sec = seconds % 60

    let timerString = String.init(format: "%02d : %02d", min, sec)
    timerLabel.text = "\(timerString)"
  }
  
  func resetTimer() {
    timerLabel.text = "00 : 00"
    seconds = 0
  }
  
  func stopTimer() {
    timer.invalidate()
  }
}



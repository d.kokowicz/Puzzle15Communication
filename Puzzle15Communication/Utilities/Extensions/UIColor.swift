//
//  RandomUIColor.swift
//  Puzzle15Communication
//
//  Created by Dominika Kokowicz on 01/06/2021.
//

import UIKit

extension UIColor {
  static var random: UIColor {
    return UIColor(red: .random(in: 0...2), green: .random(in: 0...2), blue: .random(in: 0...2), alpha: 1.0)
  }
}


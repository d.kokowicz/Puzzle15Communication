//
//  CGFLoat.swift
//  Puzzle15Communication
//
//  Created by Dominika Kokowicz on 02/06/2021.
//

import UIKit

extension CGFloat {
  func rounded(toPlaces places:Int) -> CGFloat {
    let divisor = pow(10.0, CGFloat(places))
    return (self * divisor).rounded() / divisor
  }
}





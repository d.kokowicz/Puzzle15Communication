//
//  Board.swift
//  Puzzle15Communication
//
//  Created by Dominika Kokowicz on 01/06/2021.
//

import UIKit

class BoardModel {
  var size = Constant.boardSize
  var width: CGFloat?
  var tiles: [TileModel]
  
  init(tiles: [TileModel]) {
    self.tiles = tiles
  }
  
}

//
//  Tile.swift
//  Puzzle15Communication
//
//  Created by Dominika Kokowicz on 01/06/2021.
//

import UIKit

class TileModel {
  var id: Int
  var imageTile: UIImage?
  var width: CGFloat?
  var xCenter: CGFloat?
  var yCenter: CGFloat?

  init(id: Int, imageTile: UIImage?, width: CGFloat?, xCenter: CGFloat?, yCenter: CGFloat?) {
    self.id = id
    self.imageTile = imageTile
    self.width = width
    self.xCenter = xCenter
    self.yCenter = yCenter
  }
}


